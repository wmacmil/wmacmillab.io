+++
title = "First post"
date = 2018-07-01

[taxonomies]
categories = ["Odd"]
tags = ["placeholder posts", "tag1"]
+++

Hellow world
Hello world2


{{ katex(body="\KaTeX") }}

x{{ katex(body="^{3}") }}
{{ katex(body="x^{3}") }}

$$\sqrt{x} \Leftrightarrow x^{1/2}
\quad \sqrt[3]{2}
\quad \sqrt{x^{2} + \sqrt{y}}
\quad \surd[x^2 + y^2]$$

$$ f(x) = h(x) $$

$$ \frac{A \quad b}{c} $$
$$ \frac{\Gamma \vdash \quad b}{c} $$
$$ \frac{\Gamma \vdash b \quad \Gamma \vdash  b'}{c} $$
$$ \frac{\Gamma \vdash b \quad \Gamma \vdash  b'}{c}{d} $$
$$ \frac{\Gamma \vdash b \quad \Gamma \vdash  b'}{\frac{c}{d}} $$

{{ katex(body="\LaTeX") }}

This comes from my $$\heartsuit$$

$$ \KaTeX $$

$$ \KaTeX $$
$$ \tau\epsilon\chi $$


$$ 
\begin{equation}
a^2 + b^2 = c^2
\end{equation}
$$



$$ \f{x} = \int{-\infty}^\infty \hat \f\xi\,e^{2 \pi i \xi x} \,d\xi $$

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
in culpa qui officia deserunt mollit anim id est laborum.

<!-- more -->

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
in culpa qui officia deserunt mollit anim id est laborum.
